#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "cryptedserver",
    version = "1.0.0",
    url = 'http://ondrejsika.com/docs/python-cryptedserver',
    download_url = 'https://github.com/sikaondrej/python-cryptedserver',
    license = 'GNU LGPL v.3',
    description = "Crypted socket server in Python.",
    author = 'Ondrej Sika',
    author_email = 'dev@ondrejsika.com',
    packages = find_packages(),
    install_requires = ["simplesocket", "crypto"],
    include_package_data = True,
)
