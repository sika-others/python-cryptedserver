import crypto
import pickle
from simplesocket import Server


def CryptedServer(address, handler):
    sessions = {}
    pub, pri = crypto.newkeys(256)
    def base_handler(req):
        req = pickle.loads(req)
        if req["encrypted"]:
            req["data"] = pickle.loads(crypto.decrypt_str(req["data"], pri))

        if req["data"].has_key("action") and req["data"]["action"] == "init":
            sessions[req["session"]] = crypto.load_key(req["data"]["key"])
            res = {
                "key": crypto.export_key(pub),
            }
        else:
            res = handler(req["data"])
        
        res = pickle.dumps(res)
        if req["encrypted"]:
            res = crypto.encrypt_str(res, sessions[req["session"]])
        return res
    return Server(address, base_handler)