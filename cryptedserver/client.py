import crypto
import random
import pickle
import string
from simplesocket import client_request

class CryptedServerClient:
    def crypted_request(self, data):
        req = {
            "session": self.session_key,
            "encrypted": True,
            "data": crypto.encrypt_str(pickle.dumps(data), self.ser_pub),
        }

        return pickle.loads(crypto.decrypt_str(client_request(self.address, pickle.dumps(req)), self.pri))

    def standart_request(self, data):
        req = {
            "session": self.session_key,
            "encrypted": False,
            "data": data,
        }
        return pickle.loads(client_request(self.address, pickle.dumps(req)))

    def __init__(self, address, key_length=255, session_key_length=8):
        self.address = address
        self.pub, self.pri = crypto.newkeys(key_length)
        self.session_key = "".join([random.choice(string.ascii_letters) for xxx in xrange(session_key_length)])

        res = self.standart_request({
            "action": "init",
            "key": crypto.export_key(self.pub),
        })
        self.ser_pub = crypto.load_key(res["key"])
