cryptedserver
=============

Crypted socket server in Python.

Authors
-------
*  Ondrej Sika, <http://ondrejsika.com>, dev@ondrejsika.com

Source
------
* Documentation: <http://ondrejsika.com/docs/python-cryptedserver>
* Python Package Index: <http://pypi.python.org/pypi/cryptedserver>
* GitHub: <https://github.com/sikaondrej/python-cryptedserver>

Documentation
=============

Instalation
-----------

Instalation is very simple over pip.
    # pip install cryptedserver

server
------

    from cryptedserver import CryptedServer

    try:
        import sys
        port = int(sys.argv[1])
    except IndexError:
        port = 9000

    def handler(req):
        print req
        if req.has_key("msg"):
            return "> "+req["msg"]
        return "No message recieved."

    CryptedServer(("localhost", port), handler).serve_forever()

client
------

    from cryptedserver import CryptedServerClient

    try:
        import sys
        port = int(sys.argv[1])
    except IndexError:
        port = 9000

    address = ("localhost", port)

    socket = CryptedServerClient(address)
    print socket.encrypted_request({"msg": "Hello"})
    print socket.encrypted_request({})